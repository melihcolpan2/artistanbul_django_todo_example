#!/usr/bin/python
# -*- coding: utf-8 -*-

import json

import markdown
from django.core.handlers.wsgi import WSGIRequest
from django.db.models import F
from django.http import HttpResponse, JsonResponse

from .middlewares import validator
from .models import Todo, User
from .validations import (
    validate_get_todos,
    validate_post_todo,
    validate_update_todo,
)


def index(request):
    html = markdown.markdown(open("README.rst", "r").read())
    main_html = open("templates/index.html", "r").read()
    main_css = open("templates/main.css", "r").read()
    return HttpResponse(main_html.format(main_css, html), status=200)


@validator(validate_get_todos)
def get_todos(request: WSGIRequest):
    """

    - The user_id parameter is located in query parameter or body.
    Usually, the user_id is usually carried to session or with a jwt token.
    Here it is sent this way for demonstration only.

    :param request: Django view request.
    :return: This method returns items depending on the user.

    """

    user_id = request.GET.get("user_id")
    fields = request.GET.get("fields").replace(" ", "")[1:-1].split(",")

    query = Todo.objects.filter(user_id=user_id).values(*fields)
    return JsonResponse({"data": list(query)})


@validator(validate_post_todo)
def create_todo(request: WSGIRequest):
    """

    - The user_id parameter is located in query parameter or body.
    Usually, the user_id is usually carried to session or with a jwt token.
    Here it is sent this way for demonstration only.
    - User presence has been checked, since it will get an error
    if there is no user here, but not on other routes.

    :param request: Django view request.
    :return: This method is used to create a new to-do.

    """

    _in = json.loads(request.body.decode("utf-8"))
    user_id = _in.get("user_id")

    check_user = User.objects.filter(id=user_id).exists()
    if not check_user:
        return HttpResponse("User not found.", status=404)

    Todo.objects.create(**_in)
    return HttpResponse(status=201)


def get_todo_detail(request: WSGIRequest, todo_id: int):
    """

    - The user_id parameter is located in query parameter or body.
    Usually, the user_id is usually carried to session or with a jwt token.
    Here it is sent this way for demonstration only.
    - The presence of the user or to-do id has not been checked.
    ince it will not give an error if it is not found, it is
    considered not to create extra queries and traffic.
    - All parameters are returned in the details.

    :param request: Django view request.
    :param todo_id: To-do ID.
    :return: This method returns all the details of a to-do.

    """

    user_id = request.GET.get("user_id")
    query = (
        Todo.objects.filter(id=todo_id, user_id=user_id)
        .values(
            "id",
            "title",
            "content",
            "created_at",
            "updated_at",
            "is_completed",
            user_name=F("user__name"),
            user_surname=F("user__surname"),
        )
        .get()
    )

    return JsonResponse({"data": query})


@validator(validate_update_todo)
def update_todo(request: WSGIRequest, todo_id: int):
    """

    - The user_id parameter is located in query parameter or body.
    Usually, the user_id is usually carried to session or with a jwt token.
    Here it is sent this way for demonstration only.
    - The presence of the user or to-do id has not been checked.
    Since it will not give an error if it is not found, it is
    considered not to create extra queries and traffic.
    - Only title, content and is_completed parameters can be updated.
    These parameters are specified under validations.

    :param request: Django view request.
    :param todo_id: To-do ID.
    :return: This method allows to-do's title, content, and completion
    status to be updated.

    """

    _in = json.loads(request.body.decode("utf-8"))
    user_id = request.GET.get("user_id")

    Todo.objects.filter(id=todo_id, user_id=user_id).update(**_in)
    return HttpResponse(status=204)


def delete_todo(request: WSGIRequest, todo_id: int):
    """

    - The user_id parameter is located in query parameter or body.
    Usually, the user_id is usually carried to session or with a jwt token.
    Here it is sent this way for demonstration only.
    - The presence of the user or to-do id has not been checked.
    Since it will not give an error if it is not found, it is
    considered not to create extra queries and traffic.

    :param request: Django view request.
    :param todo_id: To-do ID.
    :return: This method enables the related user's to-do to be deleted.

    """

    user_id = request.GET.get("user_id")

    Todo.objects.filter(id=todo_id, user_id=user_id).delete()
    return HttpResponse(status=202)
