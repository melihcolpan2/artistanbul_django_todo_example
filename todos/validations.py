#!/usr/bin/python
# -*- coding: utf-8 -*-

from cerberus import Validator

from .models import Todo


def validate_get_todos(_in):
    schema = {
        "user_id": {
            "type": ["integer", "list"],
            "required": True,
            "nullable": False,
        },
        "fields": {"type": "list", "required": True, "nullable": False},
    }

    v = Validator(schema)
    valid = v.validate(_in)

    # Compare input fields and model fields, if any delta return error.
    fields = _in.get("fields")[0].replace(" ", "")[1:-1].split(",")
    model_fields = [field.name for field in Todo._meta.fields]
    model_fields.append("user_id")

    if list(set(fields) - set(model_fields)):
        valid = False
        v.errors["fields"] = "You have extra parameters."

    return valid, v.errors


def validate_post_todo(_in):
    schema = {
        "title": {"type": ["string", "list"], "required": True, "nullable": False},
        "content": {"type": ["string", "list"], "required": True, "nullable": False},
        "user_id": {"type": "integer", "required": True, "nullable": False},
    }

    v = Validator(schema)
    valid = v.validate(_in)

    return valid, v.errors


def validate_update_todo(_in):
    schema = {
        "title": {"type": ["string", "list"], "required": False, "nullable": False},
        "content": {"type": ["string", "list"], "required": False, "nullable": False},
        "is_completed": {"type": "boolean", "required": False, "nullable": False},
    }

    v = Validator(schema)
    valid = v.validate(_in)

    return valid, v.errors
