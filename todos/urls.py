#!/usr/bin/python
# -*- coding: utf-8 -*-

from django.urls import path

from . import views

urlpatterns = [
    path("todos/create/", views.create_todo, name="create_todo"),
    path("todos/<int:todo_id>/update/", views.update_todo, name="update_todo"),
    path("todos/<int:todo_id>/delete/", views.delete_todo, name="delete_todo"),
    path("todos/<int:todo_id>/", views.get_todo_detail, name="get_todo_detail"),
    path("todos/", views.get_todos, name="get_todos"),
    path("", views.index, name="index"),
]
