#!/usr/bin/python
# -*- coding: utf-8 -*-

from django.contrib import admin

# Register your models here.
from todos.models import Todo, User

admin.site.register(User)
admin.site.register(Todo)
