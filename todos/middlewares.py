#!/usr/bin/python
# -*- coding: utf-8 -*-

import json
from json import JSONDecodeError

from django.http import HttpResponse


def validator(schema=None):
    def decorator(func):
        def wrapper(*args, **kwargs):
            request = args[0]

            if request.method == "GET":
                _in = dict(request.GET)
                valid, errors = schema(_in)
                if not valid:
                    return HttpResponse(errors, status=422)

            elif request.method == "POST" or request.method == "PUT":
                try:
                    _in = json.loads(request.body.decode("utf-8"))
                except JSONDecodeError:
                    return HttpResponse("Input JSON not serializable.", status=422)
                else:
                    valid, errors = schema(_in)
                    if not valid:
                        return HttpResponse(errors, status=422)

            return func(*args, **kwargs)

        return wrapper

    return decorator
