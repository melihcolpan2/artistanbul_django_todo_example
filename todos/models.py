#!/usr/bin/python
# -*- coding: utf-8 -*-

from django.db import models


class User(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=48)
    surname = models.CharField(max_length=48)


class Todo(models.Model):
    id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=200)
    content = models.TextField(max_length=1000)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    is_completed = models.BooleanField(default=False, blank=False)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
