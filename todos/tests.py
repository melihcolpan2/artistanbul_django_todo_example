#!/usr/bin/python
# -*- coding: utf-8 -*-

from django.test import RequestFactory, TestCase

from .models import Todo, User


class TodoTestCase(TestCase):
    def setUp(self):
        self.factory = RequestFactory()

        self.user = User.objects.create(id=1, name="Test", surname="Test")
        self.todo = Todo.objects.create(
            id=1, title="Test Todo Title", content="Test todo content.", user=self.user
        )
        self.does_not_user_id = 0

    def test_get_todos(self):
        user_id = self.user.id
        fields = "[id, title, content]"

        request = self.client.get(f"/todos/?user_id={user_id}&fields={fields}")
        result = request.json()

        self.assertEqual(200, request.status_code)
        self.assertIsNotNone(result)
        self.assertSetEqual(
            set(fields.replace(" ", "")[1:-1].split(",")), set(list(result["data"][0]))
        )

    def test_post_todo(self):
        data = {
            "title": "Test Title",
            "content": "Test content.",
            "user_id": self.user.id,
        }
        request = self.client.post(
            "/todos/create/",
            data=data,
            content_type="application/json",
        )
        self.assertEqual(201, request.status_code)

    def test_get_todo_detail(self):
        user_id = self.user.id
        todo_id = self.todo.id
        fields = [
            "id",
            "title",
            "content",
            "created_at",
            "updated_at",
            "is_completed",
            "user_name",
            "user_surname",
        ]

        request = self.client.get(f"/todos/{todo_id}/?user_id={user_id}")
        result = request.json()

        self.assertEqual(200, request.status_code)
        self.assertSetEqual(set(fields), set(list(result["data"].keys())))

    def test_update_todo(self):
        user_id = self.user.id
        todo_id = self.todo.id
        data = {
            "title": "Updated Test Title",
            "content": "Updated Test content.",
            "is_completed": True,
        }
        request = self.client.put(
            f"/todos/{todo_id}/update/?user_id={user_id}",
            data=data,
            content_type="application/json",
        )
        updated_todo = Todo.objects.filter(id=todo_id).get()

        self.assertEqual(204, request.status_code)
        self.assertEqual(data.get("title"), updated_todo.title)
        self.assertEqual(data.get("content"), updated_todo.content)
        self.assertEqual(data.get("is_completed"), updated_todo.is_completed)

    def test_delete_todo(self):
        user_id = self.user.id
        todo_id = self.todo.id

        request = self.client.delete(f"/todos/{todo_id}/delete/?user_id={user_id}")
        check_todo = Todo.objects.filter(id=todo_id).exists()

        self.assertEqual(202, request.status_code)
        self.assertFalse(check_todo)

    def test_get_todos_wrong_parameters(self):
        user_id = self.user.id
        fields = ["wrong_parameter", "title", "content"]

        request = self.client.get(f"/todos/?user_id={user_id}&fields={fields}")

        self.assertEqual(422, request.status_code)

    def test_post_todo_does_not_exist_user_id(self):
        data = {
            "title": "Test Title",
            "content": "Test content.",
            "user_id": self.does_not_user_id,
        }
        request = self.client.post(
            "/todos/create/",
            data=data,
            content_type="application/json",
        )
        self.assertEqual(404, request.status_code)

    def test_post_todo_with_no_data(self):
        request = self.client.post("/todos/create/")
        self.assertEqual(422, request.status_code)

    def test_post_todo_with_wrong_parameters(self):
        data = {"title": "Test Title"}
        request = self.client.post(
            "/todos/create/",
            data=data,
            content_type="application/json",
        )
        self.assertEqual(422, request.status_code)

    def test_index(self):
        request = self.client.post("/")
        self.assertEqual(200, request.status_code)
