CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation and Run
 * Coverage
 * Maintainers

INTRODUCTION
------------

This project is a to-do example application made for ArtIstanbul.
It includes a backend section. It consists of a user and to-do model
and is based on bringing, updating and deleting these with certain
filters. The tests cover the codes 100%. The coverage library was
used for test coverage. Since it is not a complete project, some
parameters such as user_id have been taken directly from the user.
For this reason, the queries to whom the to-do belongs were not
shaped but received from the user. Readme file has been provided
as index page. The project is containerized for Docker. The necessary
explanations are given in the build and run section.

At the same time, the following project was realized by building
on the to-do example project. It serves people by keeping To-Do
data client-based. There are many more features to be added. These
features will be added over time:

 * Project Source: <https://github.com/melihcolpan/django-todo-example>

 * Live Demo: <http://opia.work/>

REQUIREMENTS
------------

This module requires the following modules:

 * Cerberus v1.3.2 (<https://docs.python-cerberus.org/en/stable/>)
 * coverage v5.3.1 (<https://coverage.readthedocs.io/>)
 * Django v3.1.5 (<https://www.djangoproject.com/>)
 * Markdown v3.3.3 (<https://python-markdown.github.io/>)
 * flake8 v3.8.4 (<https://flake8.pycqa.org/>)


INSTALLATION AND RUN
--------------------

The fastest way is to try it after getting the dockerfile build and running it. Follow the steps below. Installation of Docker: <https://www.docker.com/>.

`
$ docker build -t todo-example .
`

`
$ docker run -idt --name todo-example -p 8000:8000 todo-example
`

COVERAGE
-----------
The coverage library was used while checking the coverage of the tests. Below is how to use it.

`
$ coverage run --source='todos' manage.py test todos
`

`
$ coverage report
`

In addition, the coverage report can be displayed in html form. With the following command, an htmlcov folder is created in the project folder. The whole report can be viewed with index.html in this folder.

`
$ coverage html
`


MAINTAINERS
-----------

Current maintainers:

 * Melih Çolpan - <http://www.melihcolpan.com/>
